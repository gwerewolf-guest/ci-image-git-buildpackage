#!/bin/bash -e

. /usr/share/gitlab-ci-common

# these are all required
! test -z "$CI_PROJECT_URL"
! test -z "$CI_JOB_ID"
! test -z "$CI_COMMIT_SHA"
! test -z "$CI_COMMIT_REF_NAME"

if [ "$CI_JOB_NAME" != "pages" ]; then
    echo "$0 must be run in a job called 'pages'!"
    exit 1
fi

if [ "$CI_JOB_STAGE" != "deploy" ]; then
    echo "$0 must be run in a job in the 'deploy' stage!"
    exit 1
fi

JOB_URL="$CI_PROJECT_URL/-/jobs/$CI_JOB_ID"
COMMIT_URL="$CI_PROJECT_URL/commit/$CI_COMMIT_SHA"
BRANCH_URL="$CI_PROJECT_URL/commits/$CI_COMMIT_REF_NAME"

depends="aptly gnupg"
$apt_get update
$apt_get install --no-install-recommends $depends
apt-mark auto $depends

# build apt repo from freshly built .debs
aptly repo create autobuilt || true
aptly repo add autobuilt ../*.deb
DEB_BUILD_ARCH=`dpkg --print-architecture`
aptly publish repo --skip-signing --distribution autobuilt --architectures all,$DEB_BUILD_ARCH autobuilt \
    || aptly publish update --skip-signing --architectures all,$DEB_BUILD_ARCH autobuilt

rm -rf public
cp -a ~/.aptly/public public
    # ease debugging since directory indices are disabled
cd public
printf "<html><head><title>apt source for %s</title></head><body><h1>" $CI_PROJECT_PATH > index.html
date >> index.html
printf '</h1><ul><li>branch <a href="%s">%s</a></li>' $BRANCH_URL $CI_COMMIT_REF_NAME >> index.html
printf '<li>commit <a href="%s">%s</a></li>' $COMMIT_URL $CI_COMMIT_SHA >> index.html
printf '<li><a href="%s">%s</a></li></ul>\n' $JOB_URL $JOB_URL >> index.html
find | xargs printf "<code>%s</code><br/>\n" >> index.html
echo "</ul></body></html>" >> index.html
